import json
import logging
import os

import numpy as np
import paho.mqtt.client as mqtt

import light

logging.basicConfig(level=logging.DEBUG)

logger = logging.getLogger(__name__)

state_topic = os.environ['STATE_TOPIC']
command_topic = os.environ['COMMAND_TOPIC']
availability_topic = os.environ['AVAIL_TOPIC']


def state(client: mqtt.Client, light_controller: light.Light):
    st = {
        'brightness': light_controller.brightness,
        'color_temp': light_controller.temperature,
        'color': {
            'r': light_controller.rgb[0],
            'g': light_controller.rgb[1],
            'b': light_controller.rgb[2],
            'x': light_controller.xy[0],
            'y': light_controller.xy[1],
            'h': light_controller.hs[0],
            's': light_controller.hs[1]
        },
        'state': 'ON' if light_controller.power else 'OFF'
    }

    st = json.dumps(st)
    logger.info(st)

    client.publish(state_topic, st, retain=True, qos=1)


def command(client: mqtt.Client, light_controller: light.Light, message: mqtt.MQTTMessage):
    comm = message.payload.decode('utf-8')
    logger.info(comm)

    comm = json.loads(comm)

    if 'state' in comm:
        light_controller.power = comm['state'] != 'OFF'

    if 'brightness' in comm:
        light_controller.brightness = comm['brightness']

    if 'color_temp' in comm:
        light_controller.temperature = comm['color_temp']

    if 'color' in comm:
        color = comm['color']

        if 'r' in color and 'g' in color and 'b' in color:
            rgb = np.array([color['r'], color['g'], color['b']])
            light_controller.rgb = rgb

        if 'x' in color and 'y' in color:
            xy = np.array([color['x'], color['y']])
            light_controller.xy = xy

        if 'h' in color and 's' in color:
            hs = np.array([color['h'], color['s']])
            light_controller.hs = hs

    state(client, light_controller)


def on_connect(client: mqtt.Client, light_controller: light.Light, flags, reason, properties):
    logger.info(reason)
    logger.info(properties)
    client.publish(availability_topic, 'online', retain=True, qos=1)
    client.subscribe(command_topic)


def main():
    broker_address = os.environ['BROKER']
    light_controller = light.Light()

    client = mqtt.Client(client_id='home/master_bath/nightlight', userdata=light_controller, protocol=mqtt.MQTTv5)
    client.enable_logger()

    client.will_set(availability_topic, 'offline', retain=True, qos=1)
    client.on_connect = on_connect
    client.on_message = command

    client.connect(broker_address)

    state(client, light_controller)

    client.loop_forever(retry_first_connection=True)


if __name__ == '__main__':
    main()
