import colour
import numpy as np
from gpiozero import RGBLED


def interpolate_kelvin_input(kelvin):
    x0 = mired_to_kelvin(500)
    x1 = mired_to_kelvin(153)

    y0 = 2500
    y1 = 9000

    scale = (y1 - y0) / (x1 - x0)
    v = scale * (kelvin - x0) + y0

    return v


def interpolate_kelvin_output(kelvin):
    x0 = mired_to_kelvin(500)
    x1 = mired_to_kelvin(153)

    y0 = 2500
    y1 = 9000

    scale = (y1 - y0) / (x1 - x0)
    v = (1.0 / scale) * (kelvin - y0) + x0
    return v


def kelvin_to_mired(kelvin):
    return 1000000.0 / kelvin


def mired_to_kelvin(mired):
    return 1000000.0 / mired


class Light(object):
    def __init__(self):
        self.__color = np.array([1.0, 1.0, 1.0])
        self.__brightness = 1.0

        self.__power = None

        red_pin = 17
        green_pin = 27
        blue_pin = 22

        self.__led = RGBLED(red_pin, green_pin, blue_pin)
        for l in self.__led._leds:
            l.frequency = 200

        self.power = True

    def __del__(self):
        self.power = False

    @property
    def power(self):
        return self.__power

    @power.setter
    def power(self, value):
        if self.__power == value:
            return

        self.__power = value

        if value:
            self.__led.on()

            self.__dispatch()
        else:
            self.__led.off()

    @property
    def rgb(self):
        return (self.__color * 255).astype(int).tolist()

    @rgb.setter
    def rgb(self, color):
        self.__color = color.astype(np.float) / 255.0
        self.__dispatch()

    @property
    def brightness(self):
        return int(self.__brightness * 255)

    @brightness.setter
    def brightness(self, percent):
        self.__brightness = float(percent) / 255.0
        self.__dispatch()

    @property
    def temperature(self):
        XYZ = colour.sRGB_to_XYZ(self.__color, apply_decoding_cctf=False)
        xy = colour.XYZ_to_xy(XYZ)
        kelvin = colour.xy_to_CCT(xy)
        kelvin = interpolate_kelvin_output(kelvin)
        mired = kelvin_to_mired(kelvin)
        return int(mired)

    @temperature.setter
    def temperature(self, mired):
        kelvin = mired_to_kelvin(mired)
        kelvin = interpolate_kelvin_input(float(kelvin))
        xy = colour.CCT_to_xy(kelvin)
        XYZ = colour.xy_to_XYZ(xy)
        self.__color = colour.XYZ_to_sRGB(XYZ, apply_encoding_cctf=False)
        self.__dispatch()

    @property
    def hs(self):
        hsv = colour.RGB_to_HSV(self.__color)
        return int(hsv[0] * 360), int(hsv[1] * 100)

    @hs.setter
    def hs(self, hs):
        hsv = np.array([hs[0] / 360, hs[1] / 100, 1.0]).astype(np.float)
        self.__color = colour.HSV_to_RGB(hsv)
        self.__dispatch()

    @property
    def xy(self):
        XYZ = colour.sRGB_to_XYZ(self.__color, apply_decoding_cctf=False)
        xy = colour.XYZ_to_xy(XYZ)
        return xy

    @xy.setter
    def xy(self, xy):
        XYZ = colour.xy_to_XYZ(xy.astype(np.float))
        self.__color = colour.XYZ_to_sRGB(XYZ, apply_encoding_cctf=False)
        self.__dispatch()

    def __dispatch(self):
        ds = (self.__color * self.__brightness)
        ds = np.clip(ds, 0, 1)
        self.__led.color = ds
